#ifndef VAST_GLINCLUDE_H
#define VAST_GLINCLUDE_H

//////////////////////////////////////////////////////////
// Attempt to portably include Necessary OpenGL headers
//////////////////////////////////////////////////////////

#include <vast/platform.h>

#ifdef _WIN_
#include <Windows.h>
#endif

#include <GL/glew.h>
#ifdef _OSX_
    #include <OpenGL/gl.h>
#else
    #include <GL/gl.h>
#endif

#endif // VAST_GLINCLUDE_H
