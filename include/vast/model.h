#ifndef VAST_MODEL_H
#define	VAST_MODEL_H

#include <string>
#include <vector>
#include <map>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <vast/gl.h>

namespace vast {

using namespace std;
using namespace Assimp;

enum OpenGLBufferType {IndexBuffer, VertexBuffer, NormalBuffer, ColorBuffer, TexCoordBuffer, NumBufferTypes};

class LIBRARY_API Model
{
public:
    Model();
    Model(string const& file_path);
    ~Model();

    bool Load(string const& file_path);

    void RenderVAO() const;

private:
    Model(Model const&);
    void Clear();

    void GetBoundingBox(aiScene const* scene, aiVector3D *const min_vert, aiVector3D *const max_vert);
    void GetNodeBoundingBox(aiScene const* scene, aiNode const *const nd, aiVector3D *const min_vert, aiVector3D *const max_vert);

    bool LoadMeshBindVAO(aiScene const* scene);

#if defined(HAVE_PNG) || defined(HAVE_JPEG) || defined(HAVE_TIFF)
    bool LoadTextures(aiScene const* scene, string const& file_path);
#endif

    /* Material */
    struct Material {
        GLfloat diffuse[4];
        GLfloat ambient[4];
        GLfloat specular[4];
        GLfloat emission[4];
        GLfloat shininess;
        GLuint tex_count;
    };

    struct Mesh {

        /* Vertex array object (VAO) for binding vertex attribute */
        GLuint vao;

        /* Vertex buffer object */
        GLBuffer gl_buffer[NumBufferTypes];

        /* Number of faces */
        uint32_t num_faces;

        /* Bound texture id */
        uint32_t tex_id;

        /* Material parameters */
        Material mtl;
    };

    /* Texture name and object */
    map<string, GLTexture*> tex_id_map;

    vector<Mesh*> meshes;

    GLfloat scale;

};

} // namespace vast {

#endif // VAST_MODEL_H

