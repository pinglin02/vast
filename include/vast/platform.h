#ifndef VAST_PLATFORM_H
#define VAST_PLATFORM_H

#include <vast/config.h>

#ifdef _GCC_
#  define VAST_DEPRECATED __attribute__((deprecated))
#elif defined _MSVC_
#  define VAST_DEPRECATED __declspec(deprecated)
#else
#  define VAST_DEPRECATED
#endif

#ifdef _MSVC_
#define __thread __declspec(thread)
#endif //_MSVC_

#endif // VAST_PLATFORM_H
