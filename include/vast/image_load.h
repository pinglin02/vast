#ifndef VAST_IMAGE_LOAD_H
#define VAST_IMAGE_LOAD_H

#include <string>

#include <vast/platform.h>
#include <vast/image.h>

namespace vast {

enum ImageFileType
{
    ImageFileTypePpm,
    ImageFileTypeTga,
    ImageFileTypePng,
    ImageFileTypeJpg,
    ImageFileTypeTiff,
    ImageFileTypeGif,
    ImageFileTypeUnknown
};

struct PixelFormat
{
    // Previously, VideoInterface::PixFormat returned a string.
    // For compatibility, make this string convertable
    inline operator std::string() const { return format; }

    std::string  format;
    unsigned int channels;
    unsigned int channel_bits[4];
    unsigned int bpp;
    bool planar;
};

struct TypedImage : public Image<unsigned char>
{
    inline TypedImage()
        : Image()
    {
    }

    inline TypedImage(size_t w, size_t h, size_t pitch, unsigned char* ptr, PixelFormat fmt)
        : Image(w,h,pitch,ptr), fmt(fmt)
    {
    }    
    
    PixelFormat fmt;
};

struct ImageException : std::exception
{
    ImageException(std::string str) : desc(str) {}
    ImageException(std::string str, std::string detail) {
        desc = str + "\n\t" + detail;
    }
    ~ImageException() throw() {}
    const char* what() const throw() { return desc.c_str(); }
    std::string desc;
};

const PixelFormat SupportedPixelFormats[] =
{
    {"GRAY8", 1, {8}, 8, false},
    {"GRAY16LE", 1, {16}, 16, false},
    {"Y400A", 2, {8,8}, 16, false},
    {"RGB24", 3, {8,8,8}, 24, false},
    {"BGR24", 3, {8,8,8}, 24, false},
    {"YUYV422", 3, {4,2,2}, 16, false},
    {"RGBA",  4, {8,8,8,8}, 32, false},
    {"GRAY32F", 1, {32}, 32, false},
    {"",0,{0,0,0,0},0,0}
};

LIBRARY_API
PixelFormat ImageFormatFromString(const std::string& format);

LIBRARY_API
std::string FileLowercaseExtention(const std::string& filename);

LIBRARY_API
ImageFileType FileTypeMagic(const unsigned char data[], size_t bytes);

LIBRARY_API
ImageFileType FileTypeExtension(const std::string& ext);

LIBRARY_API
ImageFileType FileType(const std::string& filename);

LIBRARY_API
TypedImage LoadImage(const std::string& filename, ImageFileType file_type);

LIBRARY_API
TypedImage LoadImage(const std::string& filename);

LIBRARY_API
void FlipImageY(TypedImage& img);

LIBRARY_API
void FreeImage(TypedImage img);

} // namespace vast {

#endif // VAST_IMAGE_LOAD_H
