#ifndef VAST_IMAGE_H
#define VAST_IMAGE_H

namespace vast {

// Simple image wrapper
template<typename T>
struct Image {
    inline Image()
        : pitch(0), ptr(0), w(0), h(0)
    {
    }

    inline Image(size_t w, size_t h, size_t pitch, unsigned char* ptr)
        : pitch(pitch), ptr(ptr), w(w), h(h)
    {
    }
    
    void Dealloc()
    {
        if(ptr) {
            delete[] ptr;
            ptr = NULL;
        }
    }
    
    void Alloc(size_t w, size_t h, size_t pitch)
    {
        Dealloc();
        this->w = w;
        this->h = h;
        this->pitch = pitch;
        this->ptr = new T[h*pitch];
    }

    size_t pitch;
    T* ptr;
    size_t w;
    size_t h;
};

} // namespace vast {

#endif // VAST_IMAGE_H
