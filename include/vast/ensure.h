#ifndef VAST_ENSURE_H
#define VAST_ENSURE_H

#include <cstdio>
#include <cstdlib>

#undef VAST_ENSURE

// ENSURES are similar to ASSERTS, but they are always checked for (including in
// release builds). At the moment there are no ASSERTS in wombat which should
// only be used for checks which are performance critical.

#ifdef __GNUC__
#  define VAST_FUNCTION __PRETTY_FUNCTION__
#elif(_MSC_VER >= 1310)
#  define VAST_FUNCTION __FUNCTION__
#else
#  define VAST_FUNCTION "unknown"
#endif

#if defined(VAST_DISABLE_ENSURES)

#  define VAST_ENSURE(expr, description) ((void)0)

#elif defined(VAST_ENABLE_ENSURE_HANDLER)

namespace vast {
void ensureFailed(const char * function, const char * file, int line,
                  const char * description);
}

#define VAST_ENSURE(expr, description) ((expr)                               \
  ? ((void)0)                                                                  \
  : ::wombat::ensureFailed(VAST_FUNCTION, __FILE__, __LINE__,                \
                           (description)))
#else
namespace vast {
inline void defaultEnsure(const char * function, const char * file, int line,
                   const char * description) {
  std::printf("Wombat ensure failed in function '%s', file '%s', line %d.\n",
              function, file, line);
  std::printf("Description: %s\n",  description);
  std::abort();
}
}
#define VAST_ENSURE(expr, description) ((expr)                             \
   ? ((void)0)                                                                 \
  : wombat::defaultEnsure(VAST_FUNCTION, __FILE__, __LINE__,                 \
                          (description)))
#endif


#endif // VAST_ENSURE_H
