#ifndef VAST_GLSL_H
#define VAST_GLSL_H

#ifdef _MSC_VER
#include <stdint.h>
#define __PRETTY_FUNCTION__ __FUNCTION__
#endif

#include <iostream>
#include <typeinfo>

#include <vast/glinclude.h>
#include <vast/shaders.h>

namespace vast {

using namespace std;

#define ZERO_MEM(a) memset(a, 0, sizeof(a))

#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))

#define SAFE_DELETE(p) if (p) { delete p; p = NULL; }

static inline void Warning(char const *const format,...)
{
    va_list args;

    va_start(args, format);
    fprintf(stderr, "Warning: ");
    vfprintf(stderr, format, args);
    va_end(args);
    putc('\n', stderr);
}

static inline void FatalError(char const *const format,...)
{
    va_list args;

    va_start(args, format);
    fprintf(stderr, "Fatal Error: ");
    vfprintf(stderr, format, args);
    va_end(args);
    putc('\n', stderr);
    exit(1);
}

class LIBRARY_API GLSLProgram
{
public:

    enum ShaderSource
    {
        ShaderFile,     /* shader is read from file */
        ShaderString    /* shader is in string already */
    };

    GLSLProgram(string const& vsFile, string const& fsFile, ShaderSource const& source)
    {
      shader_vp = glCreateShader(GL_VERTEX_SHADER);
      shader_fp = glCreateShader(GL_FRAGMENT_SHADER);

      string vsText;
      string fsText;

      switch (source)
      {
      case ShaderFile:
        vsText = ShaderFileRead(vsFile.c_str());
        fsText = ShaderFileRead(fsFile.c_str());
        break;
      case ShaderString:
        vsText = vsFile;
        fsText = fsFile;
      }

      if (!vsText.size() || !fsText.size())
      {
        FatalError("Either vertex shader or fragment shader file not found.");
        return;
      }

      char const* vs_char = vsText.c_str();
      char const* fs_char = fsText.c_str();

      glShaderSource(shader_vp, 1, &vs_char, 0);
      glShaderSource(shader_fp, 1, &fs_char, 0);

      glCompileShader(shader_vp);
      ValidateShader(shader_vp, vsFile);
      glCompileShader(shader_fp);
      ValidateShader(shader_fp, fsFile);

      shader_id = glCreateProgram();
      glAttachShader(shader_id, shader_fp);
      glAttachShader(shader_id, shader_vp);
      glLinkProgram(shader_id);
      ValidateProgram(shader_id);

      isBound = false;
    }

    ~GLSLProgram()
    {
        glDetachShader(shader_id, shader_fp);
        glDetachShader(shader_id, shader_vp);

        glDeleteShader(shader_fp);
        glDeleteShader(shader_vp);
        glDeleteProgram(shader_id);
    }

    static inline char const *const ShaderFileRead(const char *fileName);

    inline void Init(const char *vsFile, const char *fsFile);

    inline void Bind()
    {
        isBound = true;
        glUseProgram(shader_id);
    }

    inline void Unbind()
    {
        isBound = false;
        glUseProgram(0);
    }

    inline unsigned int GetID()
    {
        return shader_id;
    }

    inline void ActivateTexture(char const* const param, uint8_t const tex_id)
    {
        glActiveTexture(GL_TEXTURE0+tex_id);
        glUniform1i(glGetUniformLocation(shader_id, param), tex_id);
    }

    template<typename Tp>
    inline void SetUniformOneParam(char const* const param, Tp const value)
    {
        if(isBound)
        {
            if(typeid(Tp) == typeid(float))
                glUniform1f(glGetUniformLocation(shader_id, param), value);
            else if(typeid(Tp) == typeid(int))
                glUniform1i(glGetUniformLocation(shader_id, param), value);
            else if(typeid(Tp) == typeid(unsigned int))
                glUniform1ui(glGetUniformLocation(shader_id, param), value);
            else if(typeid(Tp) == typeid(bool))
                glUniform1i(glGetUniformLocation(shader_id, param), value);
        }
        else
            FatalError("%s: Shader ID: %d is not bound!", __PRETTY_FUNCTION__, shader_id);

    }

    template<typename Tp>
    inline void SetUniformVec2(char const* const param, Tp const *const value)
    {

        if(isBound)
        {
            if(typeid(Tp) == typeid(float const*))
                glUniform2fv(glGetUniformLocation(shader_id, param), 1, (float const *)value);
            else if(typeid(Tp) == typeid(int const *))
                glUniform2iv(glGetUniformLocation(shader_id, param), 1, (int const *)value);
            else if(typeid(Tp) == typeid(unsigned int const *))
                glUniform2uiv(glGetUniformLocation(shader_id, param), 1, (unsigned int const *)value);
        }
        else
            FatalError("%s: Shader ID: %d is not bound!", __PRETTY_FUNCTION__, shader_id);

    }

    template<typename Tp>
    inline void SetUniformMatrix3(char const* const param, Tp const *const value)
    {

        if(isBound)
        {
            if(typeid(Tp) == typeid(float const *))
                glUniformMatrix3fv(glGetUniformLocation(shader_id, param), 1, false, (float const *)value);
        }
        else
            FatalError("%s: Shader ID: %d is not bound!", __PRETTY_FUNCTION__, shader_id);

    }

    template<typename Tp>
    inline void SetUniformMatrix4(char const* const param, Tp const *const value)
    {

        if(isBound)
        {
            if(typeid(Tp) == typeid(float const *))
                glUniformMatrix4fv(glGetUniformLocation(shader_id, param), 1, false, (float const *)value);
        }
        else
            FatalError("%s: Shader ID: %d is not bound!", __PRETTY_FUNCTION__, shader_id);

    }

private:
    GLSLProgram();


    inline void ValidateShader(GLuint shader, string const& file = 0);
    inline void ValidateProgram(GLuint program);

    unsigned int shader_id;
    unsigned int shader_vp;
    unsigned int shader_fp;

    // Vertex Attribute Locations
    static uint32_t const vertex_loc    = 0;
    static uint32_t const normal_loc    = 1;
    static uint32_t const texCoord_loc  = 2;

    // Uniform Bindings Points
    static uint32_t const uni_matrices_loc = 1;
    static uint32_t const uni_material_loc = 2;

    bool isBound;

};

//GLSLProgram::GLSLProgram(string const& vsFile, string const& fsFile, ShaderSource const& source)


inline char const *const GLSLProgram::ShaderFileRead(const char *fileName)
{
    char* text = NULL;

    if(fileName != NULL)
    {
        FILE *file = fopen(fileName, "rt");

        if (file != NULL)
        {
            fseek(file, 0, SEEK_END);
            int count = ftell(file);
            rewind(file);
            if (count > 0)
            {
                text = (char*)malloc(sizeof(char) * (count + 1));
                count = fread(text, sizeof(char), count, file);
                text[count] = '\0';
            }
            fclose(file);
        }
        else
            FatalError("%s: Cannot open shader file %s.", __PRETTY_FUNCTION__, fileName);
    }
    return text;
}

inline void GLSLProgram::ValidateShader(GLuint shader, string const& file)
{

    const unsigned int BUFFER_SIZE = 1024;
    char buffer[BUFFER_SIZE];
    memset(buffer, 0, BUFFER_SIZE);
    GLsizei length = 0;

    glGetShaderInfoLog(shader, BUFFER_SIZE, &length, buffer);
    if (length > 0)
        FatalError("%s: Shader %d (%s) compile error: %s", __PRETTY_FUNCTION__, shader, file.c_str(), buffer);
}

inline void GLSLProgram::ValidateProgram(GLuint program)
{
    const unsigned int BUFFER_SIZE = 1024;
    char buffer[BUFFER_SIZE];
    memset(buffer, 0, BUFFER_SIZE);
    GLsizei length = 0;

    memset(buffer, 0, BUFFER_SIZE);
    glGetProgramInfoLog(program, BUFFER_SIZE, &length, buffer);
    if (length > 0)
        cerr << "Program " << program << " link error: " << buffer << endl;

    glValidateProgram(program);
    GLint status;
    glGetProgramiv(program, GL_VALIDATE_STATUS, &status);
    if (status == GL_FALSE)
        FatalError("%s: Error in validating shader program: %s", __PRETTY_FUNCTION__, program);
}

} // namespace vast {

#endif // VAST_GLSL_H
