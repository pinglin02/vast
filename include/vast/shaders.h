#ifndef VAST_SHADERS_H
#define VAST_SHADERS_H

#include <string>

namespace vast {

using namespace std;

////////////////////////////////////////////////////////////////////////////
//  GLSL hard-coded shaders
////////////////////////////////////////////////////////////////////////////

#define GLSL(src) "#version 440 core\n" #src

string const raw_depth_vert = GLSL(
varying float depth;
void main()
{
    vec4 viewPos = gl_ModelViewMatrix * gl_Vertex;
    depth = -viewPos.z;
    gl_Position = ftransform();
});

string const raw_depth_frag = GLSL(
varying float depth;
void main(void)
{
    gl_FragData[0] = vec4(depth, depth, depth, depth);
});

string const raw_depth_contour_vert = GLSL(
varying float depth;
void main()
{
    vec4 viewPos = gl_ModelViewMatrix * gl_Vertex;
    depth = -viewPos.z;
    gl_Position = ftransform();
});

string const raw_depth_contour_frag = string("") +
"uniform sampler2D tex_fd;" +
"uniform float tex_w;" +
"uniform float tex_h;" +
"uniform float far;" +
"varying float depth;" +
"void main(void)" +
"{" +
    "gl_FragData[0] = vec4(depth, depth, depth, depth);" +
    "gl_FragData[1] = vec4(far, far, far, far);" +

    "vec2 p = gl_FragCoord.xy;" +
    "if( texture2D(tex_fd, vec2( (p.x-1.0)/tex_w, (p.y-1.0)/tex_h ) ).x == far ||" +
        "texture2D(tex_fd, vec2( (p.x)/tex_w,     (p.y-1.0)/tex_h ) ).x == far ||" +
        "texture2D(tex_fd, vec2( (p.x+1.0)/tex_w, (p.y-1.0)/tex_h ) ).x == far ||" +
        "texture2D(tex_fd, vec2( (p.x+1.0)/tex_w, (p.y)/tex_h ) ).x     == far ||" +
        "texture2D(tex_fd, vec2( (p.x+1.0)/tex_w, (p.y+1.0)/tex_h ) ).x == far ||" +
        "texture2D(tex_fd, vec2( (p.x)/tex_w,     (p.y+1.0)/tex_h ) ).x == far ||" +
        "texture2D(tex_fd, vec2( (p.x-1.0)/tex_w, (p.y+1.0)/tex_h ) ).x == far ||" +
        "texture2D(tex_fd, vec2( (p.x-1.0)/tex_w, (p.y)/tex_h ) ).x     == far)" +
        "gl_FragData[1] = vec4(0.0, 0.0, 0.0, 0.0);" +
"}";

string const render_d_tex_vert = string("") +
"varying vec2 tex_coord;" +
"void main()"+
"{" +    
    "tex_coord = gl_MultiTexCoord0.st;"
    "gl_Position = ftransform();" +
"}";

string const render_d_tex_frag = string("") +
"uniform sampler2D tex_depth;" +
"uniform float min_z;" +
"uniform float max_z;" +
"uniform float far;" +
"varying vec2 tex_coord;" +
"void main(void)" +
"{" +
    "float d = texture2D(tex_depth, tex_coord).x;" +
    "if(d == far)" +
        "gl_FragData[0] = vec4(1.0, 1.0, 1.0, 1.0);" +
    "else" +
    "{" +
        "float n_d = (d - min_z) / (max_z - min_z);" +
        "gl_FragData[0] = vec4(n_d, n_d, n_d, n_d);" +
    "}"+
"}";

string const render_ct_tex_vert = string("") +
"varying vec2 tex_coord;" +
"void main()"+
"{" +
    "tex_coord = gl_MultiTexCoord0.st;"
    "gl_Position = ftransform();" +
"}";

string const render_ct_tex_frag = string("") +
"uniform sampler2D tex_ct;" +
"uniform float far;" +
"varying vec2 tex_coord;" +
"void main(void)" +
"{" +

    "if(texture2D(tex_ct, tex_coord).x == far)" +
        "gl_FragData[0] = vec4(1.0, 1.0, 1.0, 1.0);" +
    "else " +
         "gl_FragData[0] = vec4(0.0, 0.0, 0.0, 1.0);" +

"}";

string const render_dt_tex_vert = string("") +
"varying vec2 tex_coord;" +
"void main()"+
"{" +
    "tex_coord = gl_MultiTexCoord0.st;"
    "gl_Position = ftransform();" +
"}";

string const render_dt_tex_frag = string("") +
"uniform sampler2D tex_dt;" +
"uniform float min_dt;" +
"uniform float max_dt;" +
"varying vec2 tex_coord;" +
"void main(void)" +
"{" +

    "float d = texture2D(tex_dt, tex_coord).x;" +
    "float n_d = (d - min_dt) / (max_dt - min_dt);" +
    "gl_FragData[0] = vec4(n_d, n_d, n_d, 1.0);" +

"}";



} // namespace vast {

#endif // VAST_SHADERS_H
