#include <pangolin/pangolin.h>
#include <pangolin/timer.h>

#include <vast/glsl.h>
#include <vast/model.h>

using namespace vast;
using namespace pangolin;
using namespace std;


char window_title[] = "GLSL Depth Demo";

/* For timing */
Timer timer;
int timer_lag_counter = 0;
int timer_lag_max = 60;
void SystemLoopTimeCheck()
{
    if(++timer_lag_counter%timer_lag_max == 0)
    {
        char fps[256];
        sprintf(fps, "%s: %5.3f ms", window_title, timer.Elapsed_s()*1000.0f);
//        glutSetWindowTitle(fps);
        timer_lag_counter = 0;
    }

    timer.Reset();
}

////////////////////////////////////////////////////////////////////////////
//  GLSL hard-coded shaders
////////////////////////////////////////////////////////////////////////////

string const depth_vert = string("") +
"varying float depth;" +
"uniform float near;" +
"uniform float far;" +
"void main()"+
"{" +
    "vec4 viewPos = gl_ModelViewMatrix * gl_Vertex;" +
    "depth = -viewPos.z;" +
    "gl_Position = ftransform();" +
"}";

string const depth_frag = string("") +
"varying float depth;" +
"void main(void)" +
"{" +
    "gl_FragData[0] = vec4(depth, depth, depth, depth);" +
"}";

string const norm_depth_vert = string("") +
"varying float depth;" +
"uniform float near;" +
"uniform float far;" +
"void main()"+
"{" +
    "vec4 viewPos = gl_ModelViewMatrix * gl_Vertex;" +
    "depth = (-viewPos.z-near)/(far - near);" +
    "gl_Position = ftransform();" +
"}";

string const norm_depth_frag = string("") +
"varying float depth;" +
"void main(void)" +
"{" +
    "gl_FragColor = vec4(depth, depth, depth, depth);"
"}";

int main( int argc, char* argv[] )
{

    size_t const img_w = 640;
    size_t const img_h = 480;

    float const near = 1;
    float const far = 1000;

    const int UI_WIDTH = 150;

    // Create OpenGL window in single line thanks to GLUT
    pangolin::CreateWindowAndBind(window_title, UI_WIDTH+img_w, img_h);
    cout << "Vendor: " << glGetString(GL_VENDOR) << endl;
    cout << "Renderer: " << glGetString(GL_RENDERER) << endl;
    cout << "Version: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

    glEnable(GL_DEPTH_TEST);

    // Define Camera Render Object (for view / scene browsing)
    pangolin::OpenGlRenderState cam(
                ProjectionMatrix(img_w, img_h, img_h, img_h, img_w/2.0, img_h/2.0, near, far),
                ModelViewLookAt(200, 200, 200, 0, 0, 0, AxisY));

    // Add named OpenGL viewport to window and provide 3D Handler
    View& view = pangolin::CreateDisplay()
                  .SetBounds(0.0, 1.0, Attach::Pix(UI_WIDTH), 1.0, -float(img_w)/float(img_h))
                  .SetHandler(new Handler3D(cam));

    pangolin::CreatePanel("ui")
            .SetBounds(0.0, 1.0, 0.0, Attach::Pix(UI_WIDTH));

    /* FBO initilisation */
    GlRenderBuffer rbo(img_w, img_h);
    GlTexture tex(img_w, img_h, GL_R32F, false, 0, GL_RED, GL_FLOAT);
    GlFramebuffer fbo(tex, rbo);

    // Assimp model importer
    Model model;
    model.Load(argv[1]);

    /* GLSL programs initialisation */
    GLSLProgram raw_depth(depth_vert, depth_frag, GLSLProgram::ShaderString);
    raw_depth.Bind();
    raw_depth.SetUniformOneParam("near", near);
    raw_depth.SetUniformOneParam("far", far);
    raw_depth.Unbind();

    GLSLProgram norm_depth(norm_depth_vert, norm_depth_frag, GLSLProgram::ShaderString);
    norm_depth.Bind();
    norm_depth.SetUniformOneParam("near", near);
    norm_depth.SetUniformOneParam("far", far);
    norm_depth.Unbind();

    float *const depth = new float[img_w*img_h];

    Timer timer;
    while( !pangolin::ShouldQuit() )
    {
        static Var<double> frame_rate("ui.Frame Rate");
        static Var<float> min_val("ui.Min Depth");
        static Var<float> max_val("ui.Max Depth");
        static Var<bool> check_use_cpu_gpu("ui.Use GLSL", true, false, false);

        frame_rate = 1.0f/timer.Elapsed_s();
        timer.Reset();

        if(check_use_cpu_gpu)
        {

            /* GLSL rendering in FBO for obtaining depth w.r.t camera coordinate */
            fbo.Bind();

            glClearColor(far, far, far, far); // Assume area outside the model has an undefined depth = far.
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glViewport(0, 0, img_w, img_h);

            raw_depth.Bind();

            cam.Apply();

            model.RenderVAO();

            tex.Download(depth, GL_RED, GL_FLOAT);

            raw_depth.Unbind();
            fbo.Unbind();

        }
        else
        {
            /* Render a model and read back from depth buffer and scale it to real depth */
            glClearColor(1.0, 1.0, 1.0, 1.0);
            view.ActivateScissorAndClear(cam);
            model.RenderVAO();
            glReadPixels(view.v.l, view.v.b, img_w, img_h, GL_DEPTH_COMPONENT, GL_FLOAT, depth);
            for(int i = 0; i < img_w*img_h; ++i)
            {
                float z_b = depth[i];
                float z_n = 2.0f * z_b - 1.0f;
                depth[i] = 2.0 * near * far / (far + near - z_n * (far - near));
            }
        }

        min_val = *min_element(depth, depth+img_w*img_h);
        max_val = *max_element(depth, depth+img_w*img_h);

        /* GLSL rendering for depth visualisation purpose */
        glClearColor(1.0, 1.0, 1.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        view.Activate(cam);
        norm_depth.Bind();
        model.RenderVAO();
        norm_depth.Unbind();

        // Swap frames and Process Events
        pangolin::FinishFrame();

        SystemLoopTimeCheck();
    }

    delete [] depth;

    return 0;
}
