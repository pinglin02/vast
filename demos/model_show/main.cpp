#include <pangolin/pangolin.h>
#include <pangolin/timer.h>

#include <vast/gl.h>
#include <vast/glsl.h>
#include <vast/model.h>

using namespace vast;
using namespace pangolin;
using namespace std;


char window_title[] = "Model Show";

int main( int argc, char* argv[] )
{

    if(argc != 2)
        throw std::runtime_error(string(argv[0]) + " <model file>");

    size_t const img_w = 640;
    size_t const img_h = 480;

    float const near = 1;
    float const far = 1000;

    const int UI_WIDTH = 0;

    // Create OpenGL window in single line thanks to GLUT
    pangolin::CreateWindowAndBind(window_title, UI_WIDTH+img_w, img_h);
    cout << "Vendor: " << glGetString(GL_VENDOR) << endl;
    cout << "Renderer: " << glGetString(GL_RENDERER) << endl;
    cout << "Version: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);

    // Define Camera Render Object (for view / scene browsing)
    pangolin::OpenGlRenderState cam(
                ProjectionMatrix(img_w, img_h, img_h, img_h, img_w/2.0, img_h/2.0, near, far),
                ModelViewLookAt(200, 200, 200, 0, 0, 0, AxisY));

    // Add named OpenGL viewport to window and provide 3D Handler
    View& view = pangolin::CreateDisplay()
                  .SetBounds(0.0, 1.0, Attach::Pix(UI_WIDTH), 1.0, -float(img_w)/float(img_h))
                  .SetHandler(new Handler3D(cam));

    Model model;
    model.Load(argv[1]);

    // Default hooks for exiting (Esc) and fullscreen (tab).
    while( !pangolin::ShouldQuit() )
    {

        /* GLSL rendering for depth visualisation purpose */
        glClearColor(1.0, 1.0, 1.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        view.Activate(cam);

        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);

        model.RenderVAO();

        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);

        // Swap frames and Process Events
        pangolin::FinishFrame();

    }

    return 0;
}
