#include <fstream>
#include <iostream>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

#include <vast/model.h>

#if defined(HAVE_PNG) || defined(HAVE_JPEG) || defined(HAVE_TIFF)
#include <vast/image_load.h>
#endif

namespace vast {

Model::Model()
{
}

Model::Model(string const& file_path)
{
    Load(file_path);
}

Model::~Model()
{
    Clear();
}


void Model::Clear()
{

    for(size_t i = 0; i < meshes.size(); ++i)
    {
#ifdef _OSX_
        glDeleteVertexArraysAPPLE(1, &meshes[i]->vao);
#endif
#ifdef _LINUX_
        glDeleteVertexArrays(1, &m_vao);
#endif
        delete meshes[i];
    }

    for (std::map<string, GLTexture*>::iterator itr=tex_id_map.begin(); itr!=tex_id_map.end(); ++itr)
      delete itr->second;

}

bool Model::Load(string const& file_path)
{
    // Release the previously loaded mesh (if it exists)
    Clear();

    // Check if file exists
    std::ifstream fin(file_path.c_str());
    if(!fin.fail()) {
        fin.close();
    }
    else {
        printf("Couldn't open file: %s\n", file_path.c_str());
        return false;
    }

    /* Main Assimp scene */
    Importer importer;
    aiScene const* scene = importer.ReadFile(file_path.c_str(), aiProcessPreset_TargetRealtime_MaxQuality - aiProcess_FindDegenerates);
    if(!scene)
        printf("Error parsing '%s': '%s'\n", file_path.c_str(), importer.GetErrorString());

    /* Load texture images */
    if(!LoadTextures(scene, file_path))
        return false;

    /* Upload model data to vertex array object (VAO) */
    if(!LoadMeshBindVAO(scene))
        return false;

    /* Get model bounding box */
    aiVector3D scene_min, scene_max;
    GetBoundingBox(scene, &scene_min, &scene_max);

    GLfloat tmp_len;
    tmp_len = scene_max.x - scene_min.x;
    tmp_len = scene_max.y - scene_min.y > tmp_len ? scene_max.y - scene_min.y : tmp_len;
    tmp_len = scene_max.z - scene_min.z > tmp_len ? scene_max.z - scene_min.z : tmp_len;
    scale = 1.0f / tmp_len;

    return true;
}

#if defined(HAVE_PNG) || defined(HAVE_JPEG) || defined(HAVE_TIFF)
bool Model::LoadTextures(aiScene const* scene, string const& file_path)
{

    boost::filesystem::path p(file_path);
    boost::filesystem::path dir = p.parent_path();

    /* scan scene's materials for textures */
    for(size_t m = 0; m < scene->mNumMaterials; ++m) {
        int tex_idx = 0;
        aiString tex_file;
        aiReturn tex_found = scene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, tex_idx, &tex_file);
        while (tex_found == AI_SUCCESS) {

            TypedImage img = LoadImage(dir.string() + "/" + string(tex_file.data));

            if (img.fmt.channels == 3)
                tex_id_map[tex_file.data] = new GLTexture(img.w, img.h, GL_RGB8, true, 0, GL_RGB, GL_UNSIGNED_BYTE, img.ptr);
            else if (img.fmt.channels == 4)
                tex_id_map[tex_file.data] = new GLTexture(img.w, img.h, GL_RGBA8, true, 0, GL_RGBA, GL_UNSIGNED_BYTE, img.ptr);

            tex_idx++;
            tex_found = scene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, tex_idx, &tex_file);
        }
    }

    return true;
}
#endif

bool Model::LoadMeshBindVAO(aiScene const* scene)
{

    for(size_t m = 0; m < scene->mNumMeshes; ++m)
    {

        Mesh* mesh = new Mesh();

        /* Generate Vertex Array for mesh */
#ifdef _OSX_
        glGenVertexArraysAPPLE(1, &mesh->vao);
        glBindVertexArrayAPPLE(mesh->vao);
#elif defined _LINUX_ || defined _WIN_
        glGenVertexArrays(1, &mesh->vao);
        glBindVertexArray(mesh->vao);
#endif

        aiMesh const *const ai_mesh = scene->mMeshes[m];

        /* Buffer for vertex index */
        mesh->num_faces = scene->mMeshes[m]->mNumFaces;
        unsigned int* vert_idx = new unsigned int[ai_mesh->mNumFaces*3];
        for(size_t f = 0, idx = 0; f < ai_mesh->mNumFaces; ++f, idx += 3) {
            aiFace const *const face = &ai_mesh->mFaces[f];
            if(face->mNumIndices != 3)
            {
                printf("Support triangle face aiFace->mNumIndices == 3 only!");
                return false;
            }

            copy(face->mIndices, face->mIndices+3, &vert_idx[f*3]);
        }

        mesh->gl_buffer[IndexBuffer].Reinitialise(GLElementArrayBuffer, ai_mesh->mNumFaces, GL_UNSIGNED_INT, 3, vert_idx, GL_STATIC_DRAW);
        mesh->gl_buffer[IndexBuffer].Bind();

        delete [] vert_idx;

        /* Buffer for vertex */
        if(ai_mesh->HasPositions()) {
            mesh->gl_buffer[VertexBuffer].Reinitialise(GLArrayBuffer, ai_mesh->mNumVertices, GL_FLOAT, 3, ai_mesh->mVertices, GL_STATIC_DRAW);
            mesh->gl_buffer[VertexBuffer].Bind();
            glEnableClientState(GL_VERTEX_ARRAY);
            glVertexPointer(3, GL_FLOAT, 0, 0);
        }

        /* Buffer for normal */
        if(ai_mesh->HasNormals()) {
            mesh->gl_buffer[NormalBuffer].Reinitialise(GLArrayBuffer, ai_mesh->mNumVertices, GL_FLOAT, 3, ai_mesh->mNormals, GL_STATIC_DRAW);
            mesh->gl_buffer[NormalBuffer].Bind();
            glEnableClientState(GL_NORMAL_ARRAY);
            glNormalPointer(GL_FLOAT, 0, 0);
        }

#if defined(HAVE_PNG) || defined(HAVE_JPEG) || defined(HAVE_TIFF)
        /* Buffer for vertex texture coordinates */
        if(ai_mesh->HasTextureCoords(0)) {
            float *const tex_coords = new float [2*ai_mesh->mNumVertices];
            for(size_t v = 0; v < ai_mesh->mNumVertices; ++v) {
                tex_coords[v*2]   = ai_mesh->mTextureCoords[0][v].x;
                tex_coords[v*2+1] = 1.0f - ai_mesh->mTextureCoords[0][v].y;
            }

            mesh->gl_buffer[TexCoordBuffer].Reinitialise(GLArrayBuffer, ai_mesh->mNumVertices, GL_FLOAT, 2, tex_coords, GL_STATIC_DRAW);
            mesh->gl_buffer[TexCoordBuffer].Bind();
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);
            glTexCoordPointer(2, GL_FLOAT, 0, 0);

            delete [] tex_coords;
        }
#endif

#ifdef _OSX_
        glBindVertexArrayAPPLE(0);
#elif defined _LINUX_ || defined _WIN_
        glBindVertexArray(0);
#endif

        glBindBuffer(GL_ARRAY_BUFFER,0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

        /* Materials */
        aiMaterial* mtl = scene->mMaterials[ai_mesh->mMaterialIndex];

#if defined(HAVE_PNG) || defined(HAVE_JPEG) || defined(HAVE_TIFF)
        aiString tex_path;	//contains filename of texture
        if(AI_SUCCESS == mtl->GetTexture(aiTextureType_DIFFUSE, 0, &tex_path)) {
            mesh->tex_id = tex_id_map[tex_path.data]->tid;
            mesh->mtl.tex_count = 1;
        }
        else
            mesh->mtl.tex_count = 0;
#endif

        aiColor4D diffuse(0.8f, 0.8f, 0.8f, 1.0f);
        copy((float*)&diffuse, (float*)&diffuse+4, mesh->mtl.diffuse);
        if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_DIFFUSE, &diffuse))
            copy((float*)&diffuse, (float*)&diffuse+4, mesh->mtl.diffuse);

        aiColor4D ambient(0.2f, 0.2f, 0.2f, 1.0f);
        copy((float*)&ambient, (float*)&ambient+4, mesh->mtl.ambient);
        if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_AMBIENT, &ambient))
            copy((float*)&ambient, (float*)&ambient+4, mesh->mtl.ambient);

        aiColor4D specular(0.0f, 0.0f, 0.0f, 1.0f);
        copy((float*)&specular, (float*)&specular+4, mesh->mtl.specular);
        if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_SPECULAR, &specular))
            copy((float*)&specular, (float*)&specular+4, mesh->mtl.specular);

        aiColor4D emission(0.0f, 0.0f, 0.0f, 1.0f);
        copy((float*)&emission, (float*)&emission+4, mesh->mtl.emission);
        if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_EMISSIVE, &emission))
            copy((float*)&emission, (float*)&emission+4, mesh->mtl.emission);

        float shininess = 0.0;
        unsigned int max;
        aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS, &shininess, &max);
        mesh->mtl.shininess = shininess;

        meshes.push_back(mesh);

    }

    return true;
}

void Model::GetBoundingBox(aiScene const* scene, aiVector3D *const min_vert, aiVector3D *const max_vert)
{
    min_vert->x = min_vert->y = min_vert->z =  1e10f;
    max_vert->x = max_vert->y = max_vert->z = -1e10f;
    GetNodeBoundingBox(scene, scene->mRootNode, min_vert, max_vert);
}

void Model::GetNodeBoundingBox(aiScene const* scene, aiNode const *const nd, aiVector3D *const min_vert, aiVector3D *const max_vert)
{

    for(size_t n=0; n < nd->mNumMeshes; ++n) {
        aiMesh const *const mesh = scene->mMeshes[nd->mMeshes[n]];
        for(size_t i=0; i < mesh->mNumVertices; ++i) {

            aiVector3D& tmp_vert = mesh->mVertices[i];

            min_vert->x = std::min(min_vert->x, tmp_vert.x);
            min_vert->y = std::min(min_vert->y, tmp_vert.y);
            min_vert->z = std::min(min_vert->z, tmp_vert.z);

            max_vert->x = std::max(max_vert->x, tmp_vert.x);
            max_vert->y = std::max(max_vert->y, tmp_vert.y);
            max_vert->z = std::max(max_vert->z, tmp_vert.z);
        }
    }

    for (size_t n=0; n < nd->mNumChildren; ++n)
        GetNodeBoundingBox(scene, nd->mChildren[n], min_vert, max_vert);

}

void Model::RenderVAO() const
{

    for(size_t m=0; m < meshes.size(); ++m) {

#if defined(HAVE_PNG) || defined(HAVE_JPEG) || defined(HAVE_TIFF)
        glBindTexture(GL_TEXTURE_2D, meshes[m]->tex_id);
        glEnable(GL_TEXTURE_2D);
#endif

#ifdef _OSX_
        glBindVertexArrayAPPLE(meshes[m]->vao);
#elif defined _LINUX_ || defined _WIN_
        glBindVertexArray(meshes[m]->vao);
#endif

        glDrawElements(GL_TRIANGLES, meshes[m]->num_faces*3, GL_UNSIGNED_INT, 0);

#ifdef _OSX_
        glBindVertexArrayAPPLE(0);
#elif defined _LINUX_ || defined _WIN_
        glBindVertexArray(0);
#endif

        glDisable(GL_TEXTURE_2D);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    CheckGLDieOnError();

}

} // namepsace vast {
