# - Try to find Sophus (headers only)
#
#  Sophus_FOUND - system has libPangolin
#  Sophus_INCLUDE_DIR - the libPangolin include directories

find_path(
	Sophus_INCLUDE_DIR
	NAMES sophus/sophus.hpp
	PATHS /usr/include /usr/local/include)

if(Sophus_INCLUDE_DIR)
	set(Sophus_FOUND TRUE)
endif()

if(Sophus_FOUND)
   if(NOT Sophus_FIND_QUIETLY)
	  message(STATUS "Found Sophus headers: ${Sophus_INCLUDE_DIR}/sophus")
   endif()
else()
   if(Sophus_FIND_REQUIRED)
	  message(FATAL_ERROR "Could not find Sophus")
   endif()
endif()
