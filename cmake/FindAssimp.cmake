# - Try to find Assimp
# Once done, this will define
#
#  Assimp_FOUND - system has Assimp
#  Assimp_INCLUDE_DIR - the Assimp include directories
#  Assimp_LIBRARY - link these to use Assimp

find_path(
	Assimp_INCLUDE_DIR
	NAMES assimp/mesh.h
	PATHS /usr/include /usr/local/include /opt/local/include)

find_library(
	Assimp_LIBRARY
	NAMES assimp
	PATHS /usr/lib64 /usr/lib /usr/local/lib /opt/local/lib)

if(Assimp_INCLUDE_DIR AND Assimp_LIBRARY)
	set( ASSIMP_FOUND TRUE )
endif()

if(ASSIMP_FOUND)
	if(NOT Assimp_FIND_QUIETLY)
		message(STATUS "Found Assimp headers: ${Assimp_INCLUDE_DIR}/assimp")
		message(STATUS "Found Assimp library: ${Assimp_LIBRARY}")
	endif()
else()
	if(Assimp_FIND_REQUIRED)
		message(FATAL_ERROR "Could not find Assimp")
	endif()
endif()
